#!/usr/bin/env python3

import sys

import yaml

# Functions

def dump_yaml(path):
    data   = yaml.load(open(path))
    fields = data[0].keys()

    print(' '.join('{:10}'.format(field) for field in fields))
    
    for record in data:
        print(' '.join('{:10}'.format(record.get(field)) for field in fields))

# Main Execution

if __name__ == '__main__':
    for path in sys.argv[1:]:
        dump_yaml(path)
