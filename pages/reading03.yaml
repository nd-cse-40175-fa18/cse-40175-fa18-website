title:      "Reading 03: Immigration, Work-Life Balance"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week focus on issues related to the workplace:
  competition from immigrants and the idea of a work-life balance.

  1. [H-1B Specialty Occupations, DOD Cooperative Research and Development Project Workers, and Fashion Models](https://www.uscis.gov/working-united-states/temporary-workers/h-1b-specialty-occupations-dod-cooperative-research-and-development-project-workers-and-fashion-models)

  2. [Is the H-1B Program a Cynical Attempt to Undercut American Workers?](https://www.theatlantic.com/business/archive/2017/02/the-dark-side-of-the-h-1b-program/516813/)

  3. [Commentary: The H-1B Visa Problem as IEEE-USA Sees
  It](https://spectrum.ieee.org/view-from-the-valley/at-work/tech-careers/commentary-the-h1b-problem-as-ieeeusa-sees-it)
  Also: [Four Ways to Tackle H-1B Visa
  Reform](https://spectrum.ieee.org/tech-talk/at-work/tech-careers/four-ways-to-tackle-h1b-visa-reform)

  4. [Engineers Are Leaving Trump’s America for the Canadian Dream](https://www.bloomberg.com/news/features/2018-04-20/h-1b-workers-are-leaving-trump-s-america-for-the-canadian-dream)

  5. [Why do we work so hard?](https://www.1843magazine.com/features/why-do-we-work-so-hard)

  6. [Why Women Still Can't Have It All](http://www.theatlantic.com/magazine/archive/2012/07/why-women-still-cant-have-it-all/309020/)

  7. [Inside Amazon: Wrestling Big Ideas in a Bruising Workplace](http://www.nytimes.com/2015/08/16/technology/inside-amazon-wrestling-big-ideas-in-a-bruising-workplace.html)

  8. [Maybe We All Need a Little Less Balance](http://archive.is/klBMh#selection-1958.0-1958.1)

  ### Optional: Immigration

  These readings provide some background on the [H-1B] visa and lists what
  companies are utilizing this immigration program.

  - [H-1B use skyrocketed among Bay Area tech giants](https://phys.org/news/2018-08-h-1b-skyrocketed-bay-area-tech.html)

  - [H-1B: Foreign citizens make up nearly three-quarters of Silicon Valley tech workforce, report says](https://www.mercurynews.com/2018/01/17/h-1b-foreign-citizens-make-up-nearly-three-quarters-of-silicon-valley-tech-workforce-report-says/)

  - [Top 100 H1B Visa Sponsors](http://www.myvisajobs.com/Reports/2018-H1B-Visa-Sponsor.aspx)

  These readings discuss some of the controversy regarding the use of [H-1B]
  visas.

  - [America's Mixed Feelings About Immigrant Labor: Disney-Layoffs
    Edition](http://www.theatlantic.com/business/archive/2015/06/disney-h1b-visas-immigration-layoffs/396149/)

  - [Ex-Disney IT workers sue after being asked to train their own H-1B
    replacements](http://arstechnica.com/tech-policy/2016/01/ex-disney-it-workers-sue-after-being-forced-to-train-their-own-h-1b-replacements/)

  - [Not Everyone in Tech Cheers Visa Program for Foreign Workers](https://www.nytimes.com/2017/02/05/business/h-1b-visa-tech-cheers-for-foreign-workers.html?_r=0)

  These readings argue the pros and cons of immigration as it relates to the
  technology industry, including an examination of if there is a tech-talent
  shortage.

  - [Study: Immigrants Founded 51% of U.S. Billion-Dollar
    Startups](http://archive.is/cCXTU)

  - [Immigration is about talent, not costs](https://www.aerofs.com/immigration-is-about-talent-not-costs-md/)

  - [New data on H-1B visas prove that IT outsourcers hire a lot but pay very little](https://qz.com/1041506/new-data-on-h-1b-visas-show-how-it-outsourcers-are-short-changing-workers/)

  These readings further discuss the need for reforming the [H-1B] visa program.

  - [Don't Give Silicon Valley More H1B Visas](http://www.realclearpolitics.com/articles/2017/01/14/dont_give_silicon_valley_more_h1b_visas_132795.html)

  - [Increasingly, U.S. IT workers are alleging
    discrimination](http://www.networkworld.com/article/2988324/careers/increasingly-u-s-it-workers-are-alleging-discrimination.html)

  - [Silicon Valley's "Body Shop" Secret: Highly Educated Foreign Workers Treated Like Indentured Servants](http://www.nbcbayarea.com/investigations/Silicon-Valleys-Body-Shop-Secret-280567322.html)

  These readings discuss the Trump administration's plans for the [H-1B] visa program:

  - [Trump Cracks Down on H-1B Visa Program That Feeds Silicon Valley](https://www.bloomberg.com/news/articles/2017-04-03/new-h-1b-guidelines-crack-down-on-computer-programmer-jobs)

  - [Disdainful of H-1Bs, Trump expands a different foreign worker visa](https://arstechnica.com/tech-policy/2017/07/disdainful-of-h-1bs-trump-expands-a-different-foreign-worker-visa/)

  - [Trump's H-1B Reform Is to Make Life Hell for Immigrants and Companies](https://www.bloomberg.com/news/articles/2017-11-06/trump-s-h-1b-reform-is-to-make-life-hell-for-immigrants-and-companies)

  These readings discuss some reactions to the Trump administration's plans for the [H-1B] visa program:

  - [Lets talk about the H1-B Visa](https://medium.com/@fwiwm2c/lets-talk-about-the-h1-b-visa-6e5d5def2b00)

  - [How Immigration Uncertainty Threatens America’s Tech Dominance](http://archive.is/BHfyN)

  - [Indian IT firms have been preparing for changes in H-1B visa laws for nearly a decade](https://qz.com/901292/indian-it-firms-like-wipro-tcs-and-infosys-have-been-preparing-for-changes-in-h1b-visa-laws-and-donald-trumps-america-for-several-years/?ICID=ref_fark)

  These readings discuss some recent proposals for the [H-1B] visa program:

  - [H-1B Visa Bill Reintroduced In US, Proposes Sharp Hike In Salary](http://profit.ndtv.com/news/tech-media-telecom/article-h-1b-visa-bill-reintroduced-in-us-proposes-sharp-hike-in-salary-1645337)

  - [Bill introduced in US Senate seeks to increase annual H-1B visas](https://timesofindia.indiatimes.com/business/international-business/bill-introduced-in-us-senate-seeks-to-increase-annual-h-1b-visas/articleshow/62658434.cms)

  - [U.S. tech trade groups urge Trump to let spouses of H1b holders to
    work](https://www.reuters.com/article/us-usa-immigration-tech/u-s-tech-trade-groups-urge-trump-to-let-spouses-of-h1b-holders-to-work-idUSKBN1F71D0)

  Finally, these articles discuss the Trump administration announcement regarding the end of [DACA]:

  - [Admin memo: DACA recipients should prepare for 'departure from the United States'](http://www.cnn.com/2017/09/05/politics/white-house-memo-daca-recipients-leave/index.html)

  - [From Apple to Y Combinator—tech sector denounces new “Dreamers”
    plan](https://arstechnica.com/tech-policy/2017/09/from-apple-to-y-combinator-tech-denounces-new-dreamers-immigration-plan/)

  - [Tech executives join more than 100 business leaders calling on Congress to move quickly on DACA](https://techcrunch.com/2018/01/10/tech-executives-join-more-than-100-business-leaders-calling-on-congress-to-move-quickly-on-daca/)

  [H-1B]: https://en.wikipedia.org/wiki/H-1B_visa
  [DACA]: https://en.wikipedia.org/wiki/Deferred_Action_for_Childhood_Arrivals

  ### Optional: Work-Life Balance

  These articles discuss some of the challenges of working non-stop:

  - [What happens when we work non-stop](http://www.bbc.com/capital/story/20180823-how-bad-for-you-is-working-non-stop)

  - [The compelling case for working a lot less](http://www.bbc.com/capital/story/20171204-the-compelling-case-for-working-a-lot-less)

  - [The Research Is Clear: Long Hours Backfire for People and for Companies](https://hbr.org/2015/08/the-research-is-clear-long-hours-backfire-for-people-and-for-companies)

  These articles discuss the difficulty of balancing work and family life.

  - [Why I Put My Wife's Career First](http://www.theatlantic.com/magazine/archive/2015/10/why-i-put-my-wifes-career-first/403240/)

  - [The Work-Family Imbalance](http://techcrunch.com/2015/04/04/the-work-family-imbalance/)

  - [The Revolt of Working
    Parents](https://www.theatlantic.com/business/archive/2017/01/the-new-glass-ceiling/512834/)

  These articles discuss the pressures to make work a priority in the tech industry and in America in general:

  - [In Silicon Valley, Working 9 to 5 Is for Losers](https://www.nytimes.com/2017/08/31/opinion/sunday/silicon-valley-work-life-balance-.html)

  - [I Came to San Francisco to Change My Life: I Found a Tribe of Depressed Workaholics Living on Top of One Another](http://www.alternet.org/labor/hacker-house-blues-my-life-12-programmers-2-rooms-and-one-21st-century-dream)

  - [Americans Work 25% More Than Europeans, Study Finds](https://www.bloomberg.com/news/articles/2016-10-18/americans-work-25-more-than-europeans-study-finds)

  These articles discuss the struggling with this balance in the tech industry:

  - [Silicon Valley: Perks for Some Workers, Struggles for Parents](http://www.nytimes.com/2015/04/08/upshot/silicon-valley-perks-for-some-workers-struggles-for-parents.html)

  - [Why Tech Is the Leading Industry on Parental Leave](https://www.theatlantic.com/business/archive/2016/03/tech-paid-paternity-leave/473922/)

  - [Silicon Valley's Best and Worst Jobs for New Moms (and Dads)](http://www.theatlantic.com/technology/archive/2015/03/the-best-and-worst-companies-for-new-moms-and-dads-in-silicon-valley/386384/)

  These articles focus on this issue at [Amazon]:

  - [I Had a Baby and Cancer When I Worked at Amazon. This Is My Story](https://medium.com/@jcheiffetz/i-had-a-baby-and-cancer-when-i-worked-at-amazon-this-is-my-story-9eba5eef2976#.jryxto5jz)

  - [Full memo: Jeff Bezos responds to brutal NYT story, says it doesn't represent the Amazon he leads](http://www.geekwire.com/2015/full-memo-jeff-bezos-responds-to-cutting-nyt-expose-says-tolerance-for-lack-of-empathy-needs-to-be-zero/)

  - [Amazon is piloting teams with a 30-hour workweek](https://www.washingtonpost.com/news/the-switch/wp/2016/08/26/amazon-is-piloting-teams-with-a-30-hour-work-week/)

  These articles discuss [burnout]:

  - [The Reality of Developer Burnout](https://www.kennethreitz.org/essays/the-reality-of-developer-burnout)

  - [How to Recognize Burnout Before You’re Burned Out](https://www.nytimes.com/2017/09/05/smarter-living/workplace-burnout-symptoms.html?sl_l=1&sl_rec=editorial&contentCollection=smarter-living&mData=articles%255B%255D%3Dhttps%253A%252F%252Fwww.nytimes.com%252F2017%252F09%252F05%252Fsmarter-living%252Fworkplace-burnout-symptoms.html%253Fsl_l%253D1%2526sl_rec%253Deditorial%26articles%255B%255D%3Dhttps%253A%252F%252Fwww.nytimes.com%252F2017%252F08%252F21%252Fwell%252Flive%252Ffat-bias-starts-early-and-takes-a-serious-toll.html%253Fsl_rec%253Deditorial&hp&action=click&pgtype=Homepage&clickSource=story-heading&module=smarterLiving-promo-region&region=smarterLiving-promo-region&WT.nav=smarterLiving-promo-region)

  - [Burnout and Recovery at a Tech Internship](https://code.likeagirl.io/burnout-and-recovery-at-a-tech-internship-88d59aded71d)

  [Amazon]: https://www.amazon.com

  ### Bonus

  Maybe relevant.  Maybe not.

  - [On Parenthood](http://blog.codinghorror.com/on-parenthood/)

  ## Questions

  Please write a response to one of the following questions:

  1. From the readings, what is the controversy surrounding the [H-1B Visa]
  program?  What are the arguments for and against the expansion of the
  program?  After examining the topic, where do you stand on the issues
  surrounding the program?

      - If you are in favor of expanding the use of [H-1B Visa] guest workers,
        explain why it is beneficial for the United States.  How would you
        respond to concerns about lowering of wages or loss of jobs for US
        citizens?  Shouldn't the country prioritize Americans first?

      - If you are against the use of [H-1B Visa] guest workers, explain why it
        is is not necessary or potentially harmful to the United States.  How
        would you respond to the idea that America is a [nation of immigrants]
        and that these guest workers are an effective means of tackling the
        problem of a tech talent shortage?  What do you say to some of your
        classmates who will be applying to these programs?

      - In either case, discuss whether or not you are concerned with
        competition due to foreign workers or possibly outsourcing.  Should the
        US curtail programs like the [H-1B Visa] or rescind [DACA] in order to
        prioritize Americans?  Or should it live up to its image as the [land of opportunity]?

          If you are non-US citizen, discuss how these issues impact you and
          your future plans as it relates to residency and employment in
          America.

  [H-1B Visa]:              https://www.uscis.gov/eir/visa-guide/h-1b-specialty-occupation/h-1b-visa
  [nation of immigrants]:   https://en.wikipedia.org/wiki/A_Nation_of_Immigrants
  [land of opportunity]:    https://en.wikipedia.org/wiki/American_Dream

  2. From the readings and from your experience, can men and women [have it
  all]?  That is, can parents have successful and fulfilling careers while also
  raising a family and meeting other non-work related goals?

      - What does it mean to [have it all] to you?  What examples from real
        life do you draw from in order to define what having a balance is?

      - Have you ever dealt with [burnout] or guilt over missing out on some
        portion of your life?  If so, describe how you dealt with this
        situation and what helped you overcome these difficulties.

      - What can companies do to support their workers to find this balance and
        are they ethically obliged to do so?  Was the opportunity for balance
        something that factored into your choice of career or job opportunity?
        Why or why not?

      - Finally, is this balance important to you and if so, how do you hope to
        maintain it?  What life-style changes or activities have you put in
        place to deal with finding a balance in your life and preventing
        [burnout]?

  [have it all]:    http://maxschireson.com/2014/08/05/1137/
  [startups]:       http://www.nytimes.com/2015/10/11/magazine/silicon-valleys-most-elusive-beast.html?_r=0
  [burnout]:        http://www.bbc.com/capital/story/20161116-stress-is-good-for-you-until-it-isnt?ocid=global_capital_rss
