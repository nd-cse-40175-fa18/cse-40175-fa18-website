title:      "Reading 04: Diversity, Codes of Conduct"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week focus on issues related to diversity and behavior
  in the workplace:

  1. [Why Women Don’t Code](https://quillette.com/2018/06/19/why-women-dont-code/)

  2. [Why Is Silicon Valley So Awful to Women?](https://www.theatlantic.com/magazine/archive/2017/04/why-is-silicon-valley-so-awful-to-women/517788/)

  3. [Sex and STEM: Stubborn Facts and Stubborn Ideologies](http://quillette.com/2018/02/15/sex-stem-stubborn-facts-stubborn-ideologies/)

  4. [Just Because You Always Hear It, Doesn’t Mean It’s True: Gender Difference Explanations For Disparities in Tech Are Not Supported by Science](https://mailchi.mp/ncwit/ncwit-academic-alliance-did-you-know-gender-difference-explanations-disparities-tech-not-supported-science?e=b8dffa449d)

  5. [Why Doesn't Silicon Valley Hire Black Coders?](http://www.bloomberg.com/features/2016-howard-university-coders/)

  6. [The Code of Conduct](http://jessenoller.com/blog/2012/12/7/the-code-of-conduct)

  7. [An anonymous response to dangerous FOSS Codes of Conduct](https://www.avoiceformen.com/featured/an-anonymous-response-to-dangerous-foss-codes-of-conduct/)

  8. [Exclusive: Here's The Full 10-Page Anti-Diversity Screed Circulating Internally at Google](http://gizmodo.com/exclusive-heres-the-full-10-page-anti-diversity-screed-1797564320)

  9. [Note to employees from CEO Sundar Pichai](https://www.blog.google/topics/diversity/note-employees-ceo-sundar-pichai/)

  10. [Why I Was Fired by Google](http://archive.is/CpeDX)

  ### Optional: Diversity

  These readings provide some background on the lack of diversity in the technology industry:

  - [When Women Stopped Coding](http://www.npr.org/sections/money/2014/10/21/357629765/when-women-stopped-coding)

  - [Google finally discloses its diversity record, and it's not good](http://www.pbs.org/newshour/updates/google-discloses-workforce-diversity-data-good/#.U4ZeWexaIgw.twitter)

  - [Why Prejudice Alone Doesn't Explain the Gender Gap in Science](https://blogs.scientificamerican.com/the-curious-wavefunction/why-prejudice-alone-doesnt-explain-the-gender-gap-in-science/)

  - [Why the STEM gender gap is overblown](http://www.pbs.org/newshour/making-sense/truth-women-stem-careers/)

  #### Optional: Culture

  These readings are about the possible toxic nature of tech culture:

  - [Getting free of toxic tech culture](https://blog.valerieaurora.org/2018/01/17/getting-free-of-toxic-tech-culture/)

  - [“Oh My God, This Is So F---ed Up”: Inside Silicon Valley’s Secretive, Orgiastic Dark Side](https://www.vanityfair.com/news/2018/01/brotopia-silicon-valley-secretive-orgiastic-inner-sanctum)

  - [Study Finds Women and Minorities in STEM Are Discriminated Against, to the Shock of No One](https://motherboard.vice.com/en_us/article/8xvvya/study-finds-women-and-minorities-in-stem-are-discriminated-against-pew-research-center-tech-science)

  - [The Empathy Gap in Tech: Interview with a Software Engineer](http://quillette.com/2018/01/05/empathy-gap-tech-interview-software-engineer/)

  #### Optional: Women

  These readings are about the challenges for women in the tech industry:

  - [The First Women in Tech Didn’t Leave—Men Pushed Them Out](http://archive.is/pujFJ)

  - [What's behind the tech industry's toxic masculinity problem? Inside the Valley of the Bros](https://www.theglobeandmail.com/technology/toxic-masculinity-in-silicon-valley/article35759481/)

  - [Masculine culture responsible for keeping women out of computer science, engineering](http://theconversation.com/masculine-culture-responsible-for-keeping-women-out-of-computer-science-engineering-67217)

  - [Push for Gender Equality in Tech? Some Men Say It’s Gone Too Far](https://www.nytimes.com/2017/09/23/technology/silicon-valley-men-backlash-gender-scandals.html?mtrref=undefined)

  #### Optional: Minorities

  These readings are about the challenges for ethnic and racial minorities in the tech industry:

  - [Why Tech Leadership Has A Bigger Race Than Gender Problem](https://www.wired.com/story/tech-leadership-race-problem/)

  - [The Other Side of Diversity](https://medium.com/this-is-hard/the-other-side-of-diversity-1bb3de2f053e#.7iozu532i)

  - [On Being a Black Man](https://blog.devcolor.org/on-being-a-black-man-42ecb7946fe0#.jpa0tysc8)

  - [I’m a Latino in Tech, and I Think the ‘Diversity’ Discussion Is Utterly Broken](http://observer.com/2016/10/lets-be-honest-we-have-no-idea-what-diversity-means/)

  #### Optional: Uber

  These readings are about some relatively recent events at [Uber]:

  - [Reflecting on one very, very strange year at Uber](https://www.susanjfowler.com/blog/2017/2/19/reflecting-on-one-very-strange-year-at-uber)

  - [Reflecting on Susan Fowler’s Reflections](https://medium.com/@hadrad1000/reflecting-on-susan-fowlers-reflections-e2dccb374b47)

  - [I am an Uber survivor.](https://medium.com/@amyvertino/my-name-is-not-amy-i-am-an-uber-survivor-c6d6541e632f)

  - [Inside Uber's Aggressive, Unrestrained Workplace Culture](https://www.nytimes.com/2017/02/22/technology/uber-workplace-culture.html?_r=0&mtrref=undefined&mtrref=www.nytimes.com&mtrref=www.nytimes.com&mtrref=www.nytimes.com&mtrref=www.nytimes.com&gwh=050172350207DB15F99351B7BCBC8A05&gwt=pay)

  [Uber]:   https://uber.com
  [Google]: https://www.google.com

  #### Optional: Education

  These readings are about some educational approaches for improving diversity:

  - [Most computer science majors in the U.S. are men. Not so at Harvey Mudd](http://www.latimes.com/local/lanow/la-me-ln-harvey-mudd-tech-women-adv-snap-story.html)

  - [Nerdy Strutting: How to Put Women Off the Tech Industry](http://cacm.acm.org/blogs/blog-cacm/162535-nerdy-strutting-how-to-put-women-off-the-tech-industry/fulltext)

  - [Steps Teachers Can Take to Keep Girls and Minorities in Computer Science Education](https://ww2.kqed.org/mindshift/2016/11/17/steps-teachers-can-take-to-keep-girls-and-minorities-in-computer-science-education/)

  ### Optional: Codes of Conduct

  These readings have to do with codes of conduct:

  - [Open Code of Conduct](http://todogroup.org/opencodeofconduct/)

  - [Django Code of Conduct](https://www.djangoproject.com/conduct/)

  - [Proposal: A Code of Conduct for the Go community](https://github.com/golang/proposal/blob/master/design/13073-code-of-conduct.md)

  #### Optional: Linux

  These readings are about the [Linux] community:

  - [Closing a door](http://sarah.thesharps.us/2015/10/05/closing-a-door/)

  - [Linus Torvalds defends his right to shame Linux kernel developers](https://arstechnica.com/information-technology/2013/07/linus-torvalds-defends-his-right-to-shame-linux-kernel-developers/)

  - [Kindness is Underrated](https://circleci.com/blog/kindness-is-underrated/)

  #### Optional: Political Correctness

  These readings discuss the fears of political correctness in regards to these
  codes of conduct and the push for greater diversity:

  - [Why the Open Code of Conduct Isn't for Me](http://dancerscode.com/blog/why-the-open-code-of-conduct-isnt-for-me/)

  - [The Culture Wars Have Come to Silicon Valley](https://www.nytimes.com/2017/08/08/technology/the-culture-wars-have-come-to-silicon-valley.html?mtrref=encrypted.google.com)

  - [Survey Finds Conservatives Feel Out of Place in Silicon Valley](https://www.wired.com/story/survey-finds-conservatives-feel-out-of-place-in-silicon-valley/)

  #### Optional: Google

  These readings explore the recent [Google] diversity memo:

  - [The Dirty War Over Diversity Inside Google](https://www.wired.com/story/the-dirty-war-over-diversity-inside-google/)

  - [A Googler's Would-Be Manifesto Reveals Tech's Rotten Core](https://www.theatlantic.com/technology/archive/2017/08/why-is-tech-so-awful/536052/)

  - [What James Damore Got Wrong About Gender Bias in Computer Science](https://www.wired.com/story/what-james-damore-got-wrong-about-gender-bias-in-computer-science/)

  - [Google Can’t Seem to Tolerate Diversity](http://archive.is/Hv47Y)

  [Linux]: https://kernel.org

  ## Questions

  Please write a response to one of the following questions:

  1. From the readings and in your opinion, is the lack of diversity a problem
  in the technology industry or is the gender gap overblown?  Is it something
  that needs to be addressed or is it just a (possibly unfortunate) reality?

      - If you believe it is a problem, then what are some obstacles faced by
        women and minorities?  Why do these challenges exist and how could the
        technology industry (or society in general) work to remove these
        barriers and encourage more participation from women and minorities?

      - If you don't believe it is a problem, then why do you think there is
        this recent focus on diversity?  Is it fair that there are programs
        targeted to [women](http://girlswhocode.com/) and certain
        [minorities](http://blogs.scientificamerican.com/voices/inspiring-young-men-from-minority-backgrounds-to-code/),
        but not other groups?  How would you address claims of
        [privilege](http://pgbovine.net/tech-privilege.htm)?

      - What do you make of the events at [Uber]? What is your reaction on the
        events and the aftermath and what do these events say about diversity
        in technology?

  2. From the readings and in your opinion, are [Codes of
  Conduct](https://en.wikipedia.org/wiki/Code_of_conduct) necessary for
  technology companies, organizations, and communities?  Do they serve a valid
  purpose or are they just another form of [political
  correctness](https://en.wikipedia.org/wiki/Political_correctness)?

      - What do you make of some of the codes of conduct above?  What jumps out
        at you as reasonable and what seems unreasonable?

      - What do you make of the recent [Google] memo controversy?  Are the
        arguments in the memo reasonable or unreasonable?  What do you make of
        the reaction to the memo, the firing of James Damore, and the
        subsequent lawsuits?

      - Should employees be fired for "violations of codes of conduct"?  What
        sort of speech should be tolerated within the workplace? What
        responsibilities do you believe individuals have in speaking out
        against certain types of speech or in tolerating different viewpoints?

      - Can you separate politics and beliefs from the workplace? Should you?
