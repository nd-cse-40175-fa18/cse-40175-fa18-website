title:      "Reading 05: Engineering Disasters, Whistleblowing"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week center around engineering disasters and
  [whistleblowing].

  These readings are about when mission critical systems failed disastrously.
      
  - [The Worst Computer Bugs in History: Race conditions in Therac-25](https://blog.bugsnag.com/bug-day-race-condition-therac-25/)

  - [Killed by a Machine: The Therac-25](http://hackaday.com/2015/10/26/killed-by-a-machine-the-therac-25/)

  - [The Challenger Space Shuttle Disaster, 30 Years Later](http://www.nytimes.com/interactive/2016/01/29/science/space/challenger-explosion-30-year-anniversary.html?_r=0)

  - [How Challenger Exploded, and Other Mistakes Were Made](http://motherboard.vice.com/read/how-mistakes-were-made)
  
  These readings are about two cases of [whistleblowing].
  
  - [Court OKs Firing of Boeing Computer-Security Whistleblowers](http://www.wired.com/2011/05/whistleblower-firings/)

  - [Sarbanes law doesn't protect media leaks: court](https://www.reuters.com/article/us-boeing-sarbox/sarbanes-law-doesnt-protect-media-leaks-court-idUSTRE7427SN20110503)
  
  - [U.S. Intelligence Analyst Arrested in WikiLeaks Video Probe](http://www.wired.com/2010/06/leak/#ixzz0qCt040Pf)

  - [Chelsea Manning to Be Released Early as Obama Commutes Sentence](https://www.nytimes.com/2017/01/17/us/politics/obama-commutes-bulk-of-chelsea-mannings-sentence.html?_r=3&mtrref=undefined)

  #### Optional: Therac-25

  - [An Investigation of the Therac-25 Accidents](http://www.cse.msu.edu/~cse470/Public/Handouts/Therac/Therac_1.html)

  - [An Investigation of the Therac-25 Accidents -- Part II](http://www.cse.msu.edu/~cse470/Public/Handouts/Therac/Therac_2.html)

  - [An Investigation of the Therac-25 Accidents -- Part III](http://www.cse.msu.edu/~cse470/Public/Handouts/Therac/Therac_3.html)

  - [An Investigation of the Therac-25 Accidents -- Part IV](http://www.cse.msu.edu/~cse470/Public/Handouts/Therac/Therac_4.html)

  - [An Investigation of the Therac-25 Accidents -- Part V](http://www.cse.msu.edu/~cse470/Public/Handouts/Therac/Therac_5.html)

  #### Optional: Challenger

  - [CNN: Challenger Disaster Live on CNN](https://www.youtube.com/watch?v=AfnvFnzs91s)

  - [Remembering Roger Boisjoly, Challenger disaster whistleblower (1938-2012)](http://whistleblowing.us/2012/02/remembering-roger-m-boisjoly-challenger-disaster-whistleblower-1938-2012/)

  - [Whistleblowing: What Have We Learned Since the Challenger?](http://www.nspe.org/resources/ethics/ethics-resources/other-resources/whistleblowing-what-have-we-learned-challenger)

  #### Optional: Boeing Computer Security

  - [Computer security faults put Boeing at risk](http://www.seattlepi.com/business/article/Computer-security-faults-put-Boeing-at-risk-1363700.php)

  - [Boeing Employee Fired for Discussing Computer Security Problems at Company](http://www.wired.com/2007/10/boeing-employee/)

  - [Boeing Whistleblower Firing Decision May Cut Off News Leaks](http://www.bloomberg.com/news/articles/2011-05-11/boeing-ruling-on-whistleblower-firing-may-discourage-leaks-to-news-media)

  #### Optional: Chelsea/Bradley Manning:

  - [Bradley Manning's Army of One](http://nymag.com/news/features/bradley-manning-2011-7/)

  - [Bradley Manning Sentenced to 35 Years in Prison](http://www.wired.com/2013/08/bradley-manning-sentenced/)

  - [Yes, I'll get gender surgery. But I may still be punished for my suicide attempt](https://www.theguardian.com/commentisfree/2016/sep/20/chelsea-manning-gender-surgery-suicide-attempt-punishment)


  - [In Defense of the Chelsea Manning Commutation](https://www.theatlantic.com/politics/archive/2017/01/in-defense-of-the-chelsea-manning-commutation/513455/)

  #### Optional: Literature

  - [An Enemy of the People](http://www.gutenberg.org/files/2446/2446-h/2446-h.htm)

  ## Questions

  Please write a response to **one** of the following questions:

  1. From the readings, what were the root causes of the [Therac-25] accidents?
  What are the challenges for software developers working [safety-critical
  systems], how should they approach these projects, and should they be held
  liable when accidents happen?

  2. From the readings, what were the root causes of the [Challenger] disaster?
  Was Roger Boisjoly ethical in sharing information with the public?  Was his
  company justified in retaliating against him?  What good is [whistleblowing]
  if "[i]t destroy[s] [your] career, [your] life, everything else"?

  3. From the readings, what is your opinion of [Boeing]'s handling of its
  computer security employees?  Were these workers ethical in their leaking of
  information to the public?  Was their firing rightful and warranted?  Should
  these workers have been protected under the [Whistleblower protection] laws?

  4. From the readings, what is your opinion of [Chelsea/Bradley Manning]'s
  decision to leak sensitive information to [WikiLeaks](https://wikileaks.org/)
  and her subsequent sentencing?  Is what she did ethical or did she violate
  her duty?  Should she have been protected under the [Whistleblower
  protection] laws?  Is she a revolutionary hero or a traitor?

  <div class="alert alert-info" markdown="1">
  #### <i class="fa fa-spinner"></i> Edward Snowden

  [Edward Snowden] is another figure that can be discussed in the context of
  [whistleblowing], but we will save our discussion of his story for the
  following week when we talk about security and privacy concerns.

  </div>

  [Boeing]: http://www.boeing.com/
  [Chelsea/Bradley Manning]: https://en.wikipedia.org/wiki/Chelsea_Manning
  [Edward Snowden]: https://en.wikipedia.org/wiki/Edward_Snowden
  [whistleblowing]: https://en.wikipedia.org/wiki/Whistleblower
  [whistleblower protection]: https://en.wikipedia.org/wiki/Whistleblower_protection_in_the_United_States
  [Therac-25]: https://en.wikipedia.org/wiki/Therac-25
  [safety-critical systems]: https://en.wikipedia.org/wiki/Life-critical_system
  [Challenger]: https://en.wikipedia.org/wiki/Space_Shuttle_Challenger_disaster
