title:      "Project 01: Ethics in the Movies"
icon:       fa-group
navigation: []
internal:
external:
body:       |
    ## Overview

    For this project, you are to work in groups of **2 - 4** students to
    analyze a movie through the lens of a professional engineering society code
    of ethics.

    ### Code of Ethics
    
    The first step is to read through one of the code of ethics directly
    related to your field of study:

    1. [ACM Code of Ethics](https://www.acm.org/about-acm/acm-code-of-ethics-and-professional-conduct)
    
    2. [IEEE Code of Ethics](https://www.ieee.org/about/corporate/governance/p7-8.html)
    
    3. [NSPE Code of Ethics](https://www.nspe.org/resources/ethics/code-ethics)

    ### Movie

    Next, you are to watch a science, engineering, or technology related movie
    such as:

    - [Rogue One](https://www.imdb.com/title/tt3748528/?ref_=fn_al_tt_1)
    - [Hackers](https://www.imdb.com/title/tt0113243/?ref_=fn_al_tt_1)
    - [Thank You For Smoking](https://www.imdb.com/title/tt0427944/?ref_=fn_al_tt_1)
    - [The Insider](https://www.imdb.com/title/tt0140352/?ref_=fn_al_tt_1)
    - [The Maze Runner](https://www.imdb.com/title/tt1790864/?ref_=fn_al_tt_1)
    - [Snowden](https://www.imdb.com/title/tt3774114/?ref_=fn_al_tt_1)
    - [Citizenfour](https://www.imdb.com/title/tt4044364/?ref_=fn_al_tt_1)
    - [Pirates of Silicon Valley](https://www.imdb.com/title/tt0168122/?ref_=fn_al_tt_1)
    - [The Social Network](https://www.imdb.com/title/tt1285016/?ref_=fn_al_tt_1)
    - [The Constant Gardener](https://www.imdb.com/title/tt0387131/?ref_=fn_al_tt_1)
    - [The Bridge on the River Kwai](https://www.imdb.com/title/tt0050212/?ref_=fn_al_tt_1)
    - [Flash of Genius](https://www.imdb.com/title/tt1054588/?ref_=fn_al_tt_1)
    - [Real Genius](https://www.imdb.com/title/tt0089886/?ref_=fn_al_tt_1)
    - [Silkwood](https://www.imdb.com/title/tt0086312/?ref_=fn_al_tt_1)
    - [Good Will Hunting](https://www.imdb.com/title/tt0119217/?ref_=fn_al_tt_1)
    - [Enron: The Smartest Guys in the Room](https://www.imdb.com/title/tt1016268/?ref_=fn_al_tt_2)

    You may watch another movie not on this list if it is related to STEM and
    have it cleared with the instructor.  To watch a movie, you will need to
    use [Netflix], [Amazon Video], [Hulu], [Google Play], or the [Apple Store].

    [Netflix]: https://www.netflix.com/
    [Amazon Video]: https://www.amazon.com
    [Hulu]: https://www.hulu.com
    [Google Play]: https://play.google.com/store/movies?hl=en_US
    [Apple Store]: https://www.apple.com/itunes/video/

    ### Analysis

    Once you have watched the movie, you are to record a **podcast** or
    **video** as a group that does the following:

    - Provide a brief background about the context of movie.

    - Explain the ethical or moral engineering dilemma(s) presented in the film.

    - Identify the stakeholders in each situation.

    - Analyze the actions of people involved through the lens of your
      professional society's code of ethics.

    - Discuss if the movie was good or not.

    Your recording should be between **5** and **15** minutes in length.  It
    should not be totally scripted; instead it should follow a round-robin or
    panel style discussion of the required topics above.
      
    ### Submission

    Your project is due at **noon, Tuesday, September 11**.  You should upload
    your **podcast** or **video** to a cloud hosting service such as [YouTube],
    [Google Drive], [Vimeo], or [Soundcloud].

    To submit your project, one group member should fill out the following
    [form](https://goo.gl/forms/mJit5jUlN0mXmkLi1)

    <div class="text-center">
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdBGLHM8g2EDZfm5Ot80rSHGbuZcBAwqCL8num7jgIce9G7Sw/viewform?embedded=true" width="700" height="520" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
    </div>

    [Google Drive]: https://www.google.com/drive/
    [YouTube]:      https://www.youtube.com
    [Vimeo]:        https://www.vimeo.com
    [Soundcloud]:   https://www.soundcloud.com
