title:      "Reading 01: Identity"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  ## Readings

  The readings for this week revolve around identity, culture, and the general
  [ethos] of the tech industry.  In particular, we will focus on the [Hacker
  Culture] and how that has had an impact on the perception of what it means to
  be a computer scientist or engineer.

  1. [Bridges, Software Engineering, and God](https://blog.codinghorror.com/bridges-software-engineering-and-god/)

  2. [Programmers: Stop Calling Yourselves Engineers](https://www.theatlantic.com/technology/archive/2015/11/programmers-should-not-call-themselves-engineers/414271/)

  3. [Hackers and Painters](http://www.paulgraham.com/hp.html)

  4. [Mark Zuckerberg's Letter to Investors: The Hacker Way](http://www.wired.com/2012/02/zuck-letter/)

  5. [The Capitol of Meritocracy is Silicon Valley, Not Wall Street](https://www.forbes.com/sites/timothylee/2012/06/23/dont-blame-meritocracy-for-wall-street/#1b3fbbe52946)

  6. [Silicon Valley Isn't a Meritocracy. And It's Dangerous to Hero-Worship Entrepreneurs](http://www.wired.com/2013/11/silicon-valley-isnt-a-meritocracy-and-the-cult-of-the-entrepreneur-holds-people-back)

  7. [How Silicon Valley Has Disrupted Philanthropy](https://www.theatlantic.com/technology/archive/2018/07/how-silicon-valley-has-disrupted-philanthropy/565997/)

  8. [Sean Parker: Philanthropy for Hackers](http://archive.is/uEHVn)

  **Note**: You should read the articles above for the week.  To answer one of
  the question prompts below, you may need to read some of the additional
  optional readings.

  ### Optional: Hackers

  These additional readings further explore what it means to be a hacker and
  the culture that has been built up around this image.

  1. [The Conscience of a Hacker](http://phrack.org/issues/7/3.html#article)

  2. [A Portrait of J. Random Hacker](http://bak.spc.org/dms/archive/profile.html)

  3. [The Word "Hacker"](http://paulgraham.com/gba.html) and [Great Hackers](http://www.paulgraham.com/gh.html)

  4. [How yuppies hacked the original hacker ethos](https://aeon.co/essays/how-yuppies-hacked-the-original-hacker-ethos)

  ### Optional: Meritocracy

  These additional readings further explore the notion of [meritocracy], which
  is a central tenet in the technology industry.

  1. [The Post-Meritocracy Manifesto](https://postmeritocracy.org/)

  2. [Naive meritocracy and the meanings of myth](http://reagle.org/joseph/2016/myth/myth.html)

  3. [Why hiring the ‘best’ people produces the least creative results](https://qz.com/1200448/why-hiring-the-best-people-produces-the-least-creative-results/)

  4. [The Unexotic Underclass](http://miter.mit.edu/the-unexotic-underclass/)

  [meritocracy]: https://en.wikipedia.org/wiki/Meritocracy

  ### Optional: Philanthropy

  These additional readings consider the generosity and philanthropy of some of
  tech's biggest names.

  1. [A letter to our daughter](https://www.facebook.com/notes/mark-zuckerberg/a-letter-to-our-daughter/10153375081581634)

  2. [How the Gates Foundation Reflects the Good and the Bad of "Hacker Philanthropy"](https://theintercept.com/2015/11/25/how-the-gates-foundation-reflects-the-good-and-the-bad-of-hacker-philanthropy/)

  3. [Steve Jobs, World’s Greatest Philanthropist](https://hbr.org/2011/09/steve-jobs-worlds-greatest-phi.html)

  4. [In praise of Richard Stallman, GNU's open sourcerer](https://www.theguardian.com/commentisfree/2013/oct/02/richard-stallman-gnu-open-source)

  ## Questions

  Please write a response to one of the following questions:

  1. From the readings and from your experience, is Computer Science an art,
  engineering, or science discipline? Does it matter how it is categorized or
  viewed by either its practioners or the general public?  Explain your
  thoughts and the implications of your assessment.

  2. From the readings and from your experience, what exactly is a [hacker]?
  That is, what are the key characteristics of the [hacker] archetype?  Do you
  identify with these attributes? That is, would you consider yourself a
  [hacker]?  What is your reaction to this characterization?

  3. From the readings and from your experience, is the technology industry a
  meritocracy (what does that actually mean)?  If it is, then is that a good
  thing or a bad thing?  If it is not, should it try to be?

  4. Of the following computing luminaries, who is the most moral or ethical:
    [Richard Stallman], [Bill Gates], [Steve Jobs], or [Mark Zuckerberg]?  Who
    is the least?  Discuss your reasoning and provide support for your
    positions.

      What if we changed the question to who has had the most positive effect
      on the world rather than who was the most ethical or moral.  Does that
      change your assessment of any of the persons?  Explain.

  [Richard Stallman]:   https://en.wikipedia.org/wiki/Richard_Stallman
  [Bill Gates]:         https://en.wikipedia.org/wiki/Bill_gates
  [Steve Jobs]:         https://en.wikipedia.org/wiki/Steve_Jobs
  [Mark Zuckerberg]:    https://en.wikipedia.org/wiki/Mark_Zuckerberg

  [ethos]:              https://en.wikipedia.org/wiki/Ethos
  [hacker]:             https://en.wikipedia.org/wiki/Hacker_ethic
  [Hacker Culture]: https://en.wikipedia.org/wiki/Hacker_culture
