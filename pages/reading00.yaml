title:      "Reading 00: Responsibility"
icon:       fa-book
navigation: []
internal:
external:
body:       |

  **Everyone**:

  Welcome to [CSE 40175 Ethical and Professional Issues], which is a course
  where the goal is to get you to think about the ethical and moral issues
  surrounding computing and your responsibilities as a computer scientist or
  engineer.  Unlike most of your CS classes, there will be no programming (or
  even math).  Rather the course will revolve around reading and writing and
  discussion; it will be more similar to what you find in Arts and Letters than
  in Engineering or Science.

  [CSE 40175 Ethical and Professional Issues]: https://www3.nd.edu/~pbui/teaching/cse.40175.fa18/

  ## Course Overview

  As you can see from the [course schedule], we will be going through a variety
  of ethical and professional issues and topics related to computing including:

  - Responsibility, Ethics, Identity, Hackers
  - Employment and Workplace Issues
  - Engineering Disasters, Whistleblowing
  - Privacy vs Security
  - Pervasive Computing
  - Corporate Conscience
  - Freedom of Speech
  - Artificial Intelligence
  - Intellectual Property
  - Education

  To enable robust class **discussions**, each week there is a **reading
  assignment** where you are expected to read articles, watch videos, or listen
  to podcasts, and then write a **blog** post to a question prompt (the first
  reading is below).

  To encourage creative expression and different modes of communication, you
  will also be expected to work on **5 group projects**.  Each of these
  projects will require you to reflect on the issues and topics discussed in
  class and to produce a group artifact such as:

  - Podcast
  - Infomercial
  - Infographic
  - Letter to Editor or Government Official
  - Whitepaper
  - Tutorial / How-to

  Further explanation on these group projects will be provided in the future.

  [course schedule]: https://www3.nd.edu/~pbui/teaching/cse.40175.fa18/#schedule)

  ## Readings

  The readings for the first week of class revolve around broadly exploring
  what [ethics] means in the context of engineering and what it means to be a
  [Notre Dame] computer scientist and engineer:

  1. [Engineering Ethics: The Conversation without End](https://www.nae.edu/Publications/Bridge/EngineeringEthics7377/EngineeringEthicsTheConversationwithoutEnd.aspx)

  2. [A Framework for Making Ethical Decisions](https://www.brown.edu/academics/science-and-technology-studies/framework-making-ethical-decisions)

  3. [Parable of Talents](https://www.biblegateway.com/passage/?search=Matthew+25%3A14-30&version=ESV)

  4. [To Serve Man, with Software](https://blog.codinghorror.com/to-serve-man-with-software/)

  5. [Programming, power, and responsibility](https://medium.com/bits-and-behavior/programming-power-and-responsibility-4f3b4e1d9ea8)

  These videos are also relevant:

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail text-center">
        <iframe width="434" height="244" src="https://www.youtube.com/embed/nKIu9yen5nc" frameborder="0" allowfullscreen class="img-responsive"></iframe>
        <caption>
        <h4>What Most Schools Don't Teach</h4>
        </caption>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail text-center">
        <iframe width="434" height="244" src="https://www.youtube.com/embed/_5d6rTQcU2U" frameborder="0" allowfullscreen class="img-responsive"></iframe>
        <caption>
        <h4>Uncle Ben - With Great Power Comes Great Responsibility</h4>
        </caption>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail text-center">
        <iframe width="434" height="244" src="https://www.youtube.com/embed/Bwiln7v0fdc" frameborder="0" allowfullscreen class="img-responsive"></iframe>
        <caption>
        <h4>Karlie Kloss: Coding is a Superpower</h4>
        </caption>
      </div>
    </div>
  </div>

  ### Optional

  1. [Seven themes of Catholic Social Teaching](http://www.usccb.org/beliefs-and-teachings/what-we-believe/catholic-social-teaching/seven-themes-of-catholic-social-teaching.cfm)

  2. [Why Software is Eating The World](https://a16z.com/2016/08/20/why-software-is-eating-the-world/)

  3. [Modern Medicine](http://number27.org/medicine)

  4. [Programming is not a Super-Power](https://mathblog.com/programming-is-not-a-super-power/)

  5. [The code I’m still ashamed of](https://medium.freecodecamp.com/the-code-im-still-ashamed-of-e4c021dff55e#.u4aqmqc0h)

  6. [Tech’s Ethical ‘Dark Side’: Harvard, Stanford and Others Want to Address It](https://www.nytimes.com/2018/02/12/business/computer-science-ethics-courses.html)

  7. [Silicon Valley Writes a Playbook to Help Avert Ethical Disasters](https://www.wired.com/story/ethical-os)

  8. [Ask HN: What is the most unethical thing you've done as a programmer?](https://news.ycombinator.com/item?id=17692005)

  9. [Q: Why Do Keynote Speakers Keep Suggesting That Improving Security Is Possible?](https://www.usenix.org/conference/usenixsecurity18/presentation/mickens)

  <div class="alert alert-info" markdown="1">
  #### <i class="fa fa-book"></i> Readings

  Although you are encouraged to do most of the readings (they will be
  referenced in lecture), you are expected to at the very least read the
  articles that are related to the question prompt you wish to respond to
  below.

  If you find a relevant article or video, please share them in the
  [Slack](https://nd-cse.slack.com/messages/cse-40175-fa18/) channel for
  everyone to see.

  </div>

  [ethics]: https://en.wikipedia.org/wiki/Ethics


  ## Questions

  First, create a blog using whatever platform or service of your choice.  You
  will be using this blog to post your reading responses throughout the
  semester.  Here are some possible services you can use:

  1. [Notre Dame Sites](https://sites.nd.edu/)

  2. [Wordpress](https://www.wordpress.com)

  3. [Blogger](https://www.blogger.com)

  4. [Medium](https://www.medium.com)

  5. [Weebly](https://www.weebly.com/)

  Of course, you are free to write your own blog software or host it anywhere
  you wish.

  <div class="alert alert-warning" markdown="1">
  #### <i class="fa fa-pencil"></i> Post Titles

  To make it easier on the graders, please **prefix** each blog post **title**
  with the corresponding assignment.  For instance, for this reading
  assignment, your blog post can be entitled **"Reading 00"** or something
  creative like **"Reading 00: Wait, You Can't Do That?"** or **"Reading 00:
    Computer Science is magical, but it's not magic"**.

  </div>

  Next, once you have completed the readings, please write responses to the
  **both** of the following questions:

  1. In your **first** blog post, please write a **short** introduction to who you
  are, what your interests are, why you are studying Computer Science (or
  whatever your major is), and what you hope to get out of this class.

      Additionally, in your opinion, what are the most pressing ethical and
      moral issues facing computer scientists and engineers?  Which ones are
      you particularly interested in discussing this semester?

  2. For your **second** blog post, please write a response to **one** of the
  following questions?

      1. How do you normally determine if an action is right or wrong?  What
      sort of ethical or moral framework do you depend on or utilize?  What
      sort of things do you consider?

      2. Is programming a super-power? Why or why not? What are the
      implications if it is (and what is your power)?

      3. What is your interpretation of the Parable of the Talents?  How does
      it apply to your life and your computing skills and talents?

  <div class="alert alert-info" markdown="1">
  #### <i class="fa fa-calendar"></i> Due Date

  Normally, the reading responses are due at **noon** on **Monday**.  This
  gives the instructor time to read all the post before the Tuesday class.

  For this first assignment, however, please submit both posts by **noon** on
  **Wednesday, August 22**.

  </div>

  <div class="alert alert-success" markdown="1">
  #### <i class="fa fa-gavel"></i> Grading
  
  Note, you are only required to response to **one** question prompt.  Each
  response should be between **500** - **1000** words.  Each post will be
  graded in terms of:

  - **Requirements**: Does the post meet the specified word count?  (**2
    Points**)

  - **Mechanics**: Does the post utilize reasonable grammar and style? (**2
    Points**)

  - **Content**: Does the post adequately address the prompt?  Does the post
    exhibit original and personal thoughts? (**3 Points**)

  - **Organization**: Does the post utilize the readings and other sources to
    support the writing?  Are the views and arguments in the post reasonably to
    easy read and understand? (**3 Points**)

  Of course, you may choose to response to multiple prompts if you are
  interested.
  </div>

  ## Examples

  To give you an idea of what the blog should look like and what each post
  should contain, here is a sample of blogs from the [previous
  semester](https://www3.nd.edu/~pbui/teaching/cse.40175.sp18/):

  - [Daniel Kerrigan](https://medium.com/@dkerriga)
  - [Dan Wilborn](https://danwilborn.wordpress.com/)
  - [Erin Bradford](https://ethicswitherin.wordpress.com/)
  - [Grace Bushong](https://gbushong-ethics.weebly.com/)
  - [John Westhoff](https://johnwesthoff.com/ethics/)
  - [Luis Prieb](http://sites.nd.edu/luis-prieb/ethics-blog/)
  - [Maddie Gleason](https://maddiegleason.wordpress.com/)
  - [Mike Parowski](https://mikeparowski.wordpress.com/)
  - [Taylor Rongaus](https://medium.com/@trongaus)

  ## Submission

  Once you have setup your blog, please fill out the following
  [form](https://goo.gl/forms/T7KcC0z4sYSYc0y73) to let us know where to find
  it:

  <div class="text-center">
  <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdsQC2EtA0hZq9Gsnbqz4vnI7avTIa9ThREeeSkEUaKDrAxLQ/viewform?embedded=true" width="700" height="520" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
  </div>

  <div class="alert alert-danger" markdown="1">
  #### <i class="fa fa-warning"></i> Notre Dame Login

  To view and submit the form below, you need to be logged into your Notre Dame
  Google account.  The easiest way to do this is to login to
  [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
  browser session.

  </div>

  [Notre Dame]: https://www.nd.edu

