title:      "Project 03: Whistleblowing, Privacy, Security"
icon:       fa-group
navigation: []
internal:
external:
body:       |
      ## Overview

      Over the last few weeks, we have discussed issues related to
      whistleblowing, security, and privacy.  For this third project, you are to work
      in groups of **2-4** to produce artifacts about one of the following topics:

      1. Facebook and Cambridge Analytica.

      2. Privacy Paradox.

      3. Protecting privacy in the Cloud.

      ## Option 1: Facebook + Cambridge Analytica

      Recently, a whistleblower revealed that [Cambridge Analytica] acquired
      the data of over 50 million [Facebook] users in order to micro-target
      voters in the 2016 presidential campaign.

      For this first option, you are to first investigate what exactly
      happened:

      - [ ‘I made Steve Bannon’s psychological warfare tool’: meet the data war whistleblower ](https://www.theguardian.com/news/2018/mar/17/data-war-whistleblower-christopher-wylie-faceook-nix-bannon-trump)

          - [Facebook in the Age of the Big Tech Whistleblower](https://www.wired.com/story/whistleblowers-on-cambridge-analytica-and-the-question-of-big-data/)

          - ['Utterly horrifying': ex-Facebook insider says covert data harvesting was routine](https://www.theguardian.com/news/2018/mar/20/facebook-data-cambridge-analytica-sandy-parakilas)

          - [How Cambridge Analytica’s whistleblower became Facebook’s unlikely foil](https://www.mercurynews.com/2018/03/22/how-cambridge-analyticas-whistleblower-became-facebooks-unlikely-foil/)

      - [Revealed: 50 million Facebook profiles harvested for Cambridge Analytica in major data breach](https://www.theguardian.com/news/2018/mar/17/cambridge-analytica-facebook-influence-us-election?CMP=Share_AndroidApp_Copy_to_clipboard)

          - [Leaked: Cambridge Analytica's blueprint for Trump victory](https://www.theguardian.com/uk-news/2018/mar/23/leaked-cambridge-analyticas-blueprint-for-trump-victory?CMP=Share_iOSApp_Other)

          - [Exposed: Undercover secrets of Trump’s data firm](https://www.channel4.com/news/exposed-undercover-secrets-of-donald-trump-data-firm-cambridge-analytica)

          - [Revisiting Cambridge Analytica’s Role in the Presidential Election](https://www.wbez.org/shows/note-to-self/revisiting-cambridge-analyticas-role-in-the-presidential-election/983e66e2-fb30-42dc-b38c-9429af94c31e)

          - [Not Even Cambridge Analytica Believed Its Hype](https://www.theatlantic.com/politics/archive/2018/03/cambridge-analyticas-self-own/556016/)

          - [Cambridge Analytica CEO Alexander Nix suspended amid hidden-camera expose](https://www.nbcnews.com/politics/politics-news/cambridge-analytica-ceo-alexander-nix-suspended-amid-hidden-camera-expose-n858406)

          - [Justice Department and F.B.I. Are Investigating Cambridge Analytica](https://www.nytimes.com/2018/05/15/us/cambridge-analytica-federal-investigation.html)

      - [Mark Zuckerberg Speaks Out on Cambridge Analytica Scandal](https://www.wired.com/story/mark-zuckerberg-statement-cambridge-analytica/)

          - [Facebook’s Surveillance Machine](https://www.nytimes.com/2018/03/19/opinion/facebook-cambridge-analytica.html)

          - [How Researchers Learned to Use Facebook ‘Likes’ to Sway Your Thinking](https://www.nytimes.com/2018/03/20/technology/facebook-cambridge-behavior-model.html?smid=fb-nytimes&amp;smtyp=cur)

          - [Facebook Draws Scrutiny From FTC, Congressional Committees](https://www.bloomberg.com/news/articles/2018-03-20/ftc-said-to-be-probing-facebook-for-use-of-personal-data)

      - [What’s genius for Obama is scandal when it comes to Trump](http://thehill.com/opinion/technology/379245-whats-genius-for-obama-is-scandal-when-it-comes-to-trump)

          - [No, Obama Didn’t Employ the Same Strategies as Cambridge Analytica](https://washingtonmonthly.com/2018/03/21/no-obama-didnt-employ-the-same-strategies-as-cambridge-analytica/#.WrJK9WsY5bs.twitter)

          - [Stop freaking out about Cambridge Analytica. Targeted ads are great. ](https://www.washingtonpost.com/news/act-four/wp/2018/03/22/stop-freaking-out-about-cambridge-analytica-targeted-ads-are-great/)

          - [Comparing Facebook data use by Obama, Cambridge Analytica](https://www.politifact.com/truth-o-meter/statements/2018/mar/22/meghan-mccain/comparing-facebook-data-use-obama-cambridge-analyt/)

      After your investigation, provide a group discussion (round table or
      discussion panel style) in the form of a **podcast** or **video** that
      answers the following questions:

      1. Briefly describe who [Cambridge Analytica] is, and what exactly did
      they do that is now considered so controversial.

      2. Discuss the [whistleblowers] in this case.  Were they ethical in their
      whistleblowing and in their approach to leaking information or should
      they have pursued an alternative means for addressing their concerns?

      3. Discuss the response by both [Cambridge Analytica] and [Facebook].
      How did they handle the situation? Do you find them trustworthy?

      4. Discuss whether or not this situation is a significant data "breach"
      or an over-hyped "nothing burger"?

      5. Finally, what is your overall opinion of the revelations?  Does this
      impact how you view privacy, social media, or whistleblowing?  Will you
      join [#deletefacebook] or will you continue trusting [Facebook]?  Explain.

      [Cambridge Analytica]: https://cambridgeanalytica.org/
      [Facebook]:            https://www.facebook.com/
      [#deletefacebook]:     https://techcrunch.com/2018/03/19/deletefacebook/
      [whistleblowers]:      https://en.wikipedia.org/wiki/Whistleblower

      ## Option 2: Privacy Paradox

      For the second option, your group is to listen to the [Privacy Paradox]
      series of podcasts from [Note To Self]:

      1. [Day 1: What Your Phone Knows](http://www.wnyc.org/story/privacy-paradox-day-1-challenge/)

      2. [Day 2: The Search For Your Identity](http://www.wnyc.org/story/privacy-paradox-day-2-challenge/)

      3. [Day 3: Something To Hide](http://www.wnyc.org/story/privacy-paradox-day-3-challenge/)

      4. [Day 4: Fifteen Minutes of Anonymity](http://www.wnyc.org/story/privacy-paradox-day-4-challenge/)

      5. [Day 5: Your Personal Terms of Service](http://www.wnyc.org/story/privacy-paradox-day-5-challenge/)

      6. [Privacy Paradox: Results Show](https://www.wnycstudios.org/story/privacy-paradox-results)

      As you listen to the podcasts, your group should try some of the proposed
      challenges or activities (including the [privacy personality
      survey](https://wnyc.typeform.com/to/CWAeSB).  Afterwards, your group
      should produce a results **podcast** or **video** that discusses the
      following:

      1. What is exactly the [Privacy Paradox]?  Why is it such a hot topic
      today?

      2. Describe the challenges you did and what learn about privacy.  What
      did you learn about yourself?  What was surprising or interesting or
      possibly frightening?

      3. Did any of these challenges change the way you think about privacy?
      Is privacy important to you?  Explain why or why not.

      4. Finally, discuss the trade-offs between privacy and living in our
      connected world.  Can there ever be a balance?  Do you want one?  Is
      privacy something worth fighting for or protecting?  Or is it a relic of
      a by-gone era?

      **Note**: Everyone should participate in answering each question (ie.
      round table, panel discussion style).

      [Privacy Paradox]:    https://project.wnyc.org/privacy-paradox/
      [Note to Self]:       http://www.wnyc.org/shows/notetoself

      ## Option 3: Privacy + Cloud Computing

      For the final option, your group is to replace a common cloud computing
      service with a self-hosted or self-managed replacement and then create a
      **video** that demonstrates the usage of this replacement and compares it
      to an existing cloud service.  Here is a list of possible replacment
      services (feel free to come up with your own):

      - [nextCloud]: Alternative to Dropbox, Google {Drive, Calendar, Constants, etc.}.

      - [emby]: Alternative to Plex.

      - [Kolab]: Alternative to Google Apps.

      - [Syncthing]: Alternative to Dropbox.

      - [GitLab]: Alternative to Github.

      - [KeePass]: Alternative LastPass.

      - [EtherPad]: Alternative to Google Documents.

      - [Tiny RSS]: Alternative to Google Reader (RIP).

      - [Subsonic]: Alternative to Google Play.

      - [iRedMail]: Alternative to GMail.

      - [wallabag]: Alternative to Pocket or Instapaper.

      [nextCloud]:  https://nextcloud.com/
      [emby]:       https://emby.media/
      [Syncthing]:  https://syncthing.net/
      [GitLab]:     https://about.gitlab.com/
      [KeePass]:    http://keepass.info/
      [EtherPad]:   http://etherpad.org/
      [Tiny RSS]:   https://tt-rss.org/gitlab/fox/tt-rss/wikis/home
      [Subsonic]:   http://www.subsonic.org/
      [iRedMail]:   http://www.iredmail.org/
      [wallabag]:   https://www.wallabag.org/
      [Kolab]:      https://kolab.org

      In your comparison **video**, also discuss the following questions:

      1. What trade-offs are you making when using the cloud?  Have you
      consciously evaluated these trade-offs?  What is your justification?

      2. Is it ever worth it to manage your own private cloud services?  Do you
      envision a future where you may use your own services rather than third
      party ones?

      3. Do you have the moral standing to complain about encrouchment on your
      privacy when you consciously give away your information to third party
      services?

      4. Finally, will you continue using this replacement? Explain.

      **Note**: Everyone should participate in answering each question (ie.
      round table, panel discussion style).

      ## Submission

      Your project is due at **noon, Saturday, November 3, 2018**.

      To submit your project, one group member should fill out the following
      [form](https://goo.gl/forms/xd6YmYzU58PIkJYO2)

      <div class="text-center">
        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfghT4ECxTuRZq3rtebfTdUXV1ovHN9lN1APPUcnVtJB2ED4Q/viewform?embedded=true" width="640" height="1012" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
      </div>

      <div class="alert alert-info" markdown="1">
      #### <i class="fa fa-balance-scale"></i> Grading

      Your projects will be graded on how well your group addresses the chosen
      prompt, the quality of the presentation, and the thought and reflection
      in your individual responses.

      </div>
